Bootstrap: docker
From: debian:9.12
IncludeCmd: yes

%help

Usage: singularity exec interproscan_v5.42-78.0.sif interproscan.sh

This is an InterProScan container built upon
 - interproscan-5.42-78.0
 - panther-data-14.1
 - signalp-5.0b
 - tmhmm-2.0c
 - phobius101
 - ncbi-blast-2.9.0+

#%labels
#    Maintainer Jacques
#    Version v5.42-78 (PANTHER 14.1)
    
%post

    apt-get update
    apt-get upgrade -y
    apt install -y procps elfutils pkg-config python3 bzip2 ca-certificates
    apt install -y build-essential git wget libghc-zlib-dev libghc-bzlib-dev libtbb-dev unzip python3-dev libdw-dev
    echo 'deb http://ftp.debian.org/debian stretch-backports main' | tee /etc/apt/sources.list.d/stretch-backports.list
    apt update
    apt install -y openjdk-11-jdk
    apt-get clean && \
    apt-get purge 


%environment
export LC_ALL=C
export HOME=/opt/interproscan
export PATH=/opt/interproscan/:/opt/interproscan/bin:$PATH
cd $HOME

%runscript
"$@"

